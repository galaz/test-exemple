<?php

use yii\db\Migration;

/**
 * Handles adding Categoryid to table `user`.
 */
class m170716_165520_add_Categoryid_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'categoryid', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'Categoryid');
    }
}
