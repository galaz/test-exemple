<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170716_165246_create_user_table extends Migration
 {
    public function up()
    {
       $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'auth_Key' => $this->string()->notNull(),
            
        ]);
    }

    public function down()
    {
       $this->dropTable('user');
    }

}