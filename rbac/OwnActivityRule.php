<?php
namespace app\rbac;
use app\models\User;
use yii\rbac\Rule;
use Yii; 
use yii\db\ActiveRecord;

class OwnActivityRule extends Rule
{
	public $name = 'ownActivityRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			if(isset($_GET['id'])){
			    $checkUser = User::findOne($user);
			        
			    if($checkUser->CategoryId == $_GET['id'])
			        return true;
			}
		}
		return false;
	}
}